﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using DevelopEx_TestTask.Commands;
using System.Collections.ObjectModel;
using System.Threading;

namespace DevelopEx_TestTask.ViewModels
{
    class ApplicationViewModel : INotifyPropertyChanged
    {
        private string basicUrl, basicText;
        private int maxAmountOfThreads, maxAmountOfScannedUrls;

        private ObservableCollection<PageViewModel> rootPage;

        private StartCommand startCommand;

        public string BasicUrl
        {
            get { return basicUrl; }
            set
            {
                basicUrl = value;
                OnPropertyChanged("BasicUrl");
            }
        }

        public int MaxAmountOfThreads
        {
            get { return maxAmountOfThreads; }
            set
            {
                maxAmountOfThreads = value;
                OnPropertyChanged("MaxAmountOfThreads");
            }
        }

        public string BasicText
        {
            get { return basicText; }
            set
            {
                basicText = value;
                OnPropertyChanged("BasicText");
            }
        }

        public int MaxAmountOfScannedUrls
        {
            get { return maxAmountOfScannedUrls; }
            set
            {
                maxAmountOfScannedUrls = value;
                OnPropertyChanged("MaxAmountOfScannedUrls");
            }
        }

        public ObservableCollection<PageViewModel> RootPage
        {
            get { return rootPage; }
            set
            {
                rootPage = value;
                OnPropertyChanged("RootPage");
            }
        }

        public StartCommand StartCommand
        {
            get
            {
                return startCommand ?? (startCommand = new StartCommand(obj =>
                {
                    var rootPage = new ObservableCollection<PageViewModel>();
                    var basicPageViewModel = new PageViewModel(null, BasicUrl, BasicText, maxAmountOfScannedUrls);

                    Thread myThread = new Thread(new ThreadStart(() => {
                        basicPageViewModel.Search();
                        rootPage.Add(basicPageViewModel);
                        OnPropertyChanged("RootPage");
                    }));
                    myThread.Start();
                }));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string property = "")
        {
            if (PropertyChanged == null) return;

            PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }
}
