﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace DevelopEx_TestTask
{
    class PageViewModel : INotifyPropertyChanged
    {
        private PageModel page;

        private string url, text;
        private static int amountOfLeftUrls;

        private PageViewModel parent;
        private ObservableCollection<PageViewModel> children;

        private bool containsText;

        static object locker = new object();

        public PageViewModel(PageViewModel parent, string url, string text, int maxAmountOfScannedUrls)
        {
            this.url = url;
            this.text = text;

            this.page = new PageModel(this.url);

            amountOfLeftUrls = maxAmountOfScannedUrls;

            if (parent != null)
            {
                parent.Children.Add(this);
            }
            this.children = new ObservableCollection<PageViewModel>();
        }

        public string Url
        {
            get { return url; }
            set
            {
                url = value;
                OnPropertyChanged("Url");
            }
        }

        public ObservableCollection<PageViewModel> Children
        {
            get { return this.children; }
            set
            {
                this.children = value;
                OnPropertyChanged("Children");
            }
        }

        public void Search()
        {
            SearchOnPage();
            OnPropertyChanged("Children");

            if (amountOfLeftUrls <= 0) { return; }

            amountOfLeftUrls--;
        }

        private void SearchOnPage()
        {
            this.containsText = this.page.FindTextOnPage(text);

            foreach (var link in page.FindLinks())
            {
                this.Children.Add(new PageViewModel(this, link, this.text, amountOfLeftUrls));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string property = "")
        {
            if (PropertyChanged == null) return;

            PropertyChanged(this, new PropertyChangedEventArgs(property));
        }
    }
}
