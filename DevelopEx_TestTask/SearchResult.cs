﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace DevelopEx_TestTask
{
    class SearchResult
    {
        public bool DoesTextFountOnPage { get; set; }
        public ObservableCollection<string> Sublinks { get; set; }

        public SearchResult(bool doesTextFountOnPage, ObservableCollection<string> sublinks)
        {
            DoesTextFountOnPage = doesTextFountOnPage;
            Sublinks = new ObservableCollection<string>();

            foreach (var sublink in sublinks)
            {
                Sublinks.Add(sublink);
            }
        }
    }
}
