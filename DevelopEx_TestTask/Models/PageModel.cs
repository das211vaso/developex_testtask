﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Text.RegularExpressions;
using System.IO;

namespace DevelopEx_TestTask
{
    class PageModel
    {
        private string dataObjectModel;
        private string url;

        private int amountOfFindings;

        private enum Status
        {
            Scanning,
            Found,
            NotFound,
            Error
        }

        public PageModel(string url)
        {
            this.url = url;
            this.amountOfFindings = 0;
            this.dataObjectModel = SetDataObjectModel();
        }

        public string Url
        {
            get { return url; }
        }

        public bool FindTextOnPage(string text)
        {
            string regexRule = @"(?<=\>)(.*?)(?=\<)";
            Regex myRegex = new Regex(regexRule, RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Singleline);

            foreach (Match myMatch in myRegex.Matches(dataObjectModel))
            {
                if (myMatch.Success && myMatch.Value.Contains(text))
                {
                    return true;
                }
            }

            return false;
        }

        public List<string> FindLinks()
        {
            string regexRule = "(?<=href=\")(.*?)(?=\")";
            Regex myRegex = new Regex(regexRule, RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Singleline);

            var links = new List<string>();
            int counter = 0;

            foreach (Match myMatch in myRegex.Matches(dataObjectModel))
            {
                if (myMatch.Success &&
                    LinkIsHttp(myMatch.Value) &&
                    !LinkIsResource(myMatch.Value) && 
                    !links.Contains(myMatch.Value))
                {
                    links.Add(myMatch.Value);
                    counter++;
                }
            }

            return links;
        }

        private bool LinkIsHttp(string link)
        {
            var protocols = new string[] { "http://", "https://" };

            foreach (var protocol in protocols)
            {
                if (link.StartsWith(protocol)) return true;
            }

            return false;
        }

        private bool LinkIsResource(string link)
        {
            var resourceExtensions = new string[] { ".png", ".PNG", ".ico", ".ICO", ".css", ".CSS", 
            ".html", ".HTML", ".JS", ".js", ".img", ".IMG", ".jpg", ".JPG", ".jpeg", ".JPEG", ".bmp",
            ".BMP" };

            foreach (var extension in resourceExtensions)
            {
                if (link.EndsWith(extension)) return true;
            }

            return false;
        }

        private string SetDataObjectModel()
        {
            var client = new WebClient();
            return client.DownloadString(url);
        }
    }
}
